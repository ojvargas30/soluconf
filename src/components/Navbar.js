import React from 'react';

import logo from '../img/1.ico';
import './styles/Navbar.css';
// import { makeStyles, useTheme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
// import SkipPreviousIcon from '@material-ui/icons/SkipPrevious';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
// import SkipNextIcon from '@material-ui/icons/SkipNext';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

class Navbar extends React.Component {
    render() {
        return (
            <div>
                <div className="Navbar">
                    <div className="container-fluid">
                        <a href="/" className="Navbar__brand">
                            <img src={logo} className="Navbar__brand-logo" alt="Logo" width="50" />
                            <span className="font-weight-light">Solusoft</span>
                            <span className="font-weight-bold">Conf</span>
                        </a>
                    </div>
                </div>

                {/* <div>
                    <Card>
                        <div>
                            <CardContent>
                                <Typography component="h5" variant="h5">
                                    Live From Space</Typography>
                                <Typography variant="subtitle1" color="textSecondary">
                                    Mac Miller</Typography>
                            </CardContent>
                            <div>
                                <IconButton aria-label="previous">
                                </IconButton>
                                <IconButton aria-label="play/pause">
                                    <PlayArrowIcon />
                                </IconButton>
                                <IconButton aria-label="next">

                                </IconButton>
                            </div>
                        </div>
                        <CardMedia

                            image="/static/images/cards/live-from-space.jpg"
                            title="Live from space album cover"
                        />
                    </Card>
                </div>
                <div>
                <Card>
      <CardActionArea>
        <CardMedia
          image="/static/images/cards/contemplative-reptile.jpg"
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Lizard
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
            across all continents except Antarctica
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary">
          Share
        </Button>
        <Button size="small" color="primary">
          Learn More
        </Button>
      </CardActions>
    </Card>
                </div>
                <div className="mt-5 border">
                    <Accordion>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-label="Expand"
                            aria-controls="additional-actions1-content"
                            id="additional-actions1-header"
                        >
                            <FormControlLabel
                                aria-label="Acknowledge"
                                onClick={(event) => event.stopPropagation()}
                                onFocus={(event) => event.stopPropagation()}
                                control={<Checkbox />}
                                label="I acknowledge that I should stop the click event propagation"
                            />
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography color="textSecondary">
                                The click event of the nested action will propagate up and expand the accordion unless
                                you explicitly stop it.
          </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Accordion>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-label="Expand"
                            aria-controls="additional-actions2-content"
                            id="additional-actions2-header"
                        >
                            <FormControlLabel
                                aria-label="Acknowledge"
                                onClick={(event) => event.stopPropagation()}
                                onFocus={(event) => event.stopPropagation()}
                                control={<Checkbox />}
                                label="I acknowledge that I should stop the focus event propagation"
                            />
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography color="textSecondary">
                                The focus event of the nested action will propagate up and also focus the accordion
                                unless you explicitly stop it.
          </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Accordion>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-label="Expand"
                            aria-controls="additional-actions3-content"
                            id="additional-actions3-header"
                        >
                            <FormControlLabel
                                aria-label="Acknowledge"
                                onClick={(event) => event.stopPropagation()}
                                onFocus={(event) => event.stopPropagation()}
                                control={<Checkbox />}
                                label="I acknowledge that I should provide an aria-label on each action that I add"
                            />
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography color="textSecondary">
                                If you forget to put an aria-label on the nested action, the label of the action will
                                also be included in the label of the parent button that controls the accordion
                                expansion.
          </Typography>
                        </AccordionDetails>
                    </Accordion>
                </div> */}
            </div>

        );
    }
}

export default Navbar;