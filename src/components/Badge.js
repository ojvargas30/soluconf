import React from 'react';

import './styles/Badge.css';
import confLogo from '../img/1.ico';

class Badge extends React.Component {
  render() {

    const {
      avatarUrl,
      firstName,
      lastName,
      jobTitle,
      twitter
    } = this.props;

    return (
      <div className="Badge mb-5">
        <div className="Badge__header">
          <img src={confLogo} width="65" alt="Logo de la conferencia" />
        </div>

        <div className="Badge__section-name">
          <img
            className="Badge__avatar"
            src={avatarUrl}
            alt="Avatar"
          />
          <h1>
            {firstName} <br /> {lastName}
          </h1>
        </div>

        <div className="Badge__section-info">
          <h3>{jobTitle}</h3>
          <div className>@{twitter}</div>
        </div>

        <div className="Badge__footer">#Solusoftconf</div>
      </div>
    );
  }
}

export default Badge;