import React from 'react';

import { makeStyles,Card,CardActionArea,CardActions,CardContent,CardMedia,Button,Typography }
from '@material-ui/core/';

import FlareIcon from '@material-ui/icons/Flare';

class BadgesList extends React.Component {

    classes = makeStyles({
        root: {
            maxWidth: 345,
        },
        media: {
            height: 140,
        },
    });

    render() {
        return (
            <div>
                {this.props.badges.map(badge => {
                    return (
                        <Card className={this.classes.root + ` mb-5`} key={badge.id}>
                            <CardActionArea>
                                <CardMedia
                                    className={this.classes.media}
                                    image={badge.avatarUrl}
                                    title="AvatarImage"
                                    />
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="h2">
                                    <FlareIcon/>{' '}
                                        {badge.firstName} {badge.lastName}</Typography>
                                    <Typography variant="body2" color="textSecondary" component="p">
                                        {badge.jobTitle}</Typography>
                                </CardContent>
                            </CardActionArea>
                            <CardActions>
                                <Button size="small" color="primary">
                                    {badge.email}</Button>
                                <Button size="small" color="primary">
                                    {badge.twitter}</Button>
                            </CardActions>
                        </Card>
                    );
                })}
            </div>
        );
    }
}

export default BadgesList;
