import React from 'react';

class BadgeForm extends React.Component {

  // state = {
  //   // firstName: '',
  //   // lastName: '',
  //   // email: '',
  //   // jobTitle: '',
  //   // twitter: ''
  // };

  // handleChange = e => {
  //   e.preventDefault();
  //   // console.log({
  //   //   value: e.target.value,
  //   //   name: e.target.name,
  //   // });
  //   this.setState({
  //     [e.target.name]: e.target.value
  //   })
  // }

  handleClick = e => {
    e.preventDefault();
    console.log(`Click!`);
    console.log(this.state);
  }

  render() {

    const {
      firstName,
      lastName,
      jobTitle,
      email,
      twitter
    } = this.props.formValues;

    return (
      <div className="mb-4">
        <h1>New Attendant</h1>

        <form>
          <div className="form-group">
            <label>FirstName</label>
            <input onChange={this.props.onChange} value={firstName} type="text" name="firstName" className="form-control" />
          </div>
          <div className="form-group">
            <label>Last name</label>
            <input onChange={this.props.onChange} value={lastName} type="text" name="lastName" className="form-control" />
          </div>
          <div className="form-group">
            <label>Email</label>
            <input onChange={this.props.onChange} value={email} type="email" name="email" className="form-control" />
          </div>
          <div className="form-group">
            <label>Job title</label>
            <input onChange={this.props.onChange} value={jobTitle} type="text" name="jobTitle" className="form-control" />
          </div>
          <div className="form-group">
            <label>Twitter</label>
            <input onChange={this.props.onChange} value={twitter} type="text" name="twitter" className="form-control" />
          </div>
          <button onClick={this.handleClick} className="btn btn-primary">Save</button>
        </form>

      </div>
    );
  }
}

export default BadgeForm;