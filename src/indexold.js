// const ele = document.createElement('h3')

// ele.textContent = "Hola React Soy Oscar";

// const container = document.getElementById('app')

// container.appendChild(ele);

// --- CON REACT ---
// import React from 'react'; // Para que funcione JSX
// import ReactDOM from 'react-dom';

// const element = <h1>Hola mi nombre es Oscar</h1>
// const lugar = document.getElementById('app');

// ReactDOM.render(element, lugar);
import React from 'react';
import ReactDOM from 'react-dom';
import Badge from "./components/badge";

function ele(element = '') {
    let ele = document.getElementById(element);
    return ele;
}

function eve(ele = {}, eventListener = '', func) {
    ele.addEventListener(eventListener, func);
}

const place = ele('app');

// const jsxElement = <h1>Titulo fabricado con JSX</h1>
// const reactCreateElement = React.createElement(
//     'a',
//     { href: 'https://lichess.org/', target: "_blank" },
//     'Ir a Lichess'
// );


// ReactDOM.render(jsxElement, place);
// ReactDOM.render(reactCreateElement, place);


// --- Variables en JSX ---
// let name = 'Oscar';
// const jsxVariables = <h2>Hola {name}</h2>

// const reactVariables = React.createElement('p',{}, `HOLLLLLLLAAAAAA${name}`)

// ReactDOM.render(reactVariables, place)
// ReactDOM.render(jsxVariables, place)


// --- Multiples elementos renderizados ----

const jsxMultiple = (
    <div>
        <h1>Hola Mundo</h1>
        <div class="card-columns">
            <div class="card">
                <img class="card-img-top" alt="..."></img>
                <div class="card-body">
                    <h5 class="card-title">Card title that wraps to a new line</h5>
                    <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                </div>
            </div>

            <div class="card p-3">
                <blockquote class="blockquote mb-0 card-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                    <footer class="blockquote-footer">
                        <small class="text-muted">
                            Someone famous in <cite title="Source Title">Source Title</cite>
                        </small>
                    </footer>
                </blockquote>
            </div>

            <div class="card">
                <img src="..." class="card-img-top" alt="..."></img>
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>

            <div class="card bg-primary text-white text-center p-3">
                <blockquote class="blockquote mb-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat.</p>
                    <footer class="blockquote-footer text-white">
                        <small>
                            Someone famous in <cite title="Source Title">Source Title</cite>
                        </small>
                    </footer>
                </blockquote>
            </div>

            <div class="card text-center">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This card has a regular title and short paragraphy of text below it.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>

            <div class="card">
                <img src="..." class="card-img-top" alt="..."></img>
            </div>

            <div class="card p-3 text-right">
                <blockquote class="blockquote mb-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                    <footer class="blockquote-footer">
                        <small class="text-muted">
                            Someone famous in <cite title="Source Title">Source Title</cite>
                        </small>
                    </footer>
                </blockquote>
            </div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is another card with title and supporting text below. This card has some additional content to make it slightly taller overall.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
    </div>
);

ReactDOM.render(jsxMultiple, place)

ReactDOM.render(<Badge name="Oscar" lastName="Vargas"/>, place)


