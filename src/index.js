import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import BadgeNew from "./pages/BadgeNew";
import Badges from "./pages/Badges";

function ele(element = '') {
    let ele = document.getElementById(element);
    return ele;
}

function eve(ele = {}, eventListener = '', func) {
    ele.addEventListener(eventListener, func);
}

const place = ele('app');

ReactDOM.render(<Badges />, place)